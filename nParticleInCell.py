# same imports as earlier.
from vtkmodules.vtkCommonDataModel import vtkDataSet
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.numpy_interface import dataset_adapter as dsa

# new module for ParaView-specific decorators.
from paraview.util.vtkAlgorithm import smproxy, smproperty, smdomain

@smproxy.filter(label="nParticleInCell Filter")
@smproperty.input(name="Input")
class nParticleInCell(VTKPythonAlgorithmBase):
    # the rest of the code here is unchanged
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self)

    def RequestData(self, request, inInfo, outInfo):
        # get the first input.
        #### input0=inputs[0]
        input0 = dsa.WrapDataObject(vtkDataSet.GetData(inInfo[0]))

        # computation
        volFracP=input0.CellData["kinematicCloudTheta"]*1000
        volCellP=input0.CellData["volCell"]*volFracP
        vol1P=4/3*22/7*((5e-6)**3)
        numPCell=volCellP/vol1P

        #### output.CellData.append(volCellP, 'volCellP')
        #### output.CellData.append(numPCell, 'numPCell')

        # add to output
        output = dsa.WrapDataObject(vtkDataSet.GetData(outInfo))
        output.CellData.append(volCellP, "volCellP");
        output.CellData.append(numPCell, "numPCell");
        return 1
